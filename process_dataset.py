import os

import numpy

from feat_extract.main_feat import do_extract_feats
from svs_unet.main_svs import do_svs

WIN_SIZE = 0.04
HOP_LEN = 0.02


def do_get_label(tag_path, HOP_LEN):
    label_array = []
    start_end_label = []
    label_dict = {'sing': 1, 'nosing': 0}
    with open(tag_path) as tag_f:
        tag_cont = tag_f.readlines()
        for cont_item in tag_cont:
            start, end, label = cont_item.strip('\n').split(' ')
            start_end_label.append((float(start), float(end), label_dict[label]))
    temp_flag = 0
    while temp_flag + HOP_LEN < float(start_end_label[-1][1]):
        temp_flag += HOP_LEN
        if temp_flag <= float(start_end_label[0][1]):
            label_array.append(start_end_label[0][-1])
        else:
            del (start_end_label[0])
            label_array.append(start_end_label[0][-1])

    return label_array


class DataSet_Processor:
    def __init__(self, dataset_dir, vocal_sep_method, extract_feat):
        self.dataset_dir = dataset_dir
        self.SVS = vocal_sep_method
        self.feats = extract_feat

    def do_SVS_get_feats(self):
        dataset_dict = {}
        for dir_item in ['train', 'test', 'valid']:  # 'train', 'test', 'valid'
            data_item = []
            label_item = []
            sub_dir = '%s/%s' % (self.dataset_dir, dir_item)
            for item_wav in os.listdir(sub_dir):
                if '.wav' in item_wav:
                    tag_path = '%s/%s/%s' % (self.dataset_dir, 'tags', item_wav.replace('.wav', '.lab'))
                    print('now ... ' + tag_path)
                    label_lst = do_get_label(tag_path, HOP_LEN)
                    label_item.extend(label_lst)
                    wav_path = os.path.join(sub_dir, item_wav)
                    wav_data, fs = do_svs(wav_path, self.SVS)
                    feat_array = do_extract_feats(wav_data, fs, WIN_SIZE, HOP_LEN, self.feats)
                    if len(label_lst) > feat_array.shape[0]:
                        print('error:' + tag_path)
                        data_item.append(feat_array)
                        del label_item[-len(label_lst) + feat_array.shape[0]:]
                    else:
                        data_item.append(feat_array[:len(label_lst), :])

            dataset_dict[dir_item + '_data'] = numpy.concatenate(data_item)
            dataset_dict[dir_item + '_label'] = label_item
        return dataset_dict


if __name__ == '__main__':
    label_lst = do_get_label('test.lab', HOP_LEN)
