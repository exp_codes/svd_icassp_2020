import librosa
import numpy as np
import torch
import torch.nn as nn
from librosa.core import stft, load, istft
from torch.autograd import Variable

MAX_INT16 = np.iinfo(np.int16).max
H = 512
FFT_SIZE = 1024


class U_Net(nn.Module):
    def __init__(self):
        super(U_Net, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(
                in_channels=1,
                out_channels=16,
                kernel_size=4,
                stride=2,
                padding=1,
            ),
            nn.BatchNorm2d(16),
            nn.LeakyReLU(0.2),
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, 4, 2, 1),
            nn.BatchNorm2d(32),
            nn.LeakyReLU(0.2),
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 64, 4, 2, 1),
            nn.BatchNorm2d(64),
            nn.LeakyReLU(0.2),
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(64, 128, 4, 2, 1),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2),
        )
        self.conv5 = nn.Sequential(
            nn.Conv2d(128, 256, 4, 2, 1),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2),
        )
        self.conv6 = nn.Sequential(
            nn.Conv2d(256, 512, 4, 2, 1),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2),
        )
        self.deconv1 = nn.Sequential(
            nn.ConvTranspose2d(512, 256, 4, 2, 1),
            nn.BatchNorm2d(256),
            nn.Dropout2d(0.5),
            nn.ReLU(),
        )
        self.deconv2 = nn.Sequential(
            nn.ConvTranspose2d(512, 128, 4, 2, 1),
            nn.BatchNorm2d(128),
            nn.Dropout2d(0.5),
            nn.ReLU(),
        )
        self.deconv3 = nn.Sequential(
            nn.ConvTranspose2d(256, 64, 4, 2, 1),
            nn.BatchNorm2d(64),
            nn.Dropout2d(0.5),
            nn.ReLU(),
        )
        self.deconv4 = nn.Sequential(
            nn.ConvTranspose2d(128, 32, 4, 2, 1),
            nn.BatchNorm2d(32),
            nn.ReLU(),
        )
        self.deconv5 = nn.Sequential(
            nn.ConvTranspose2d(64, 16, 4, 2, 1),
            nn.BatchNorm2d(16),
            nn.ReLU(),
        )
        self.deconv6 = nn.Sequential(
            nn.ConvTranspose2d(32, 1, 4, 2, 1),
            nn.Sigmoid(),
        )

    def forward(self, x):
        h1 = self.conv1(x)
        h2 = self.conv2(h1)
        h3 = self.conv3(h2)
        h4 = self.conv4(h3)
        h5 = self.conv5(h4)
        h6 = self.conv6(h5)
        dh = self.deconv1(h6)
        dh = self.deconv2(torch.cat([dh, h5], 1))
        dh = self.deconv3(torch.cat([dh, h4], 1))
        dh = self.deconv4(torch.cat([dh, h3], 1))
        dh = self.deconv5(torch.cat([dh, h2], 1))
        output = self.deconv6(torch.cat([dh, h1], 1))
        return output


def ComputeMask(input_mag, unet, unet_params, hard=True):
    # unet = network_pytorch.U_Net()

    unet.load_state_dict(torch.load(unet_params, map_location='cpu'))
    # unet.cuda()
    unet.eval()

    input_mag = torch.FloatTensor(input_mag[np.newaxis, np.newaxis, 1:, :])
    # input_mag = input_mag.cuda()
    input_mag = Variable(input_mag)

    mask = unet(input_mag).cpu().data.numpy()[0, 0, :, :]
    mask = np.vstack((np.zeros(mask.shape[1], dtype="float32"), mask))
    if hard:
        hard_mask = np.zeros(mask.shape, dtype="float32")
        hard_mask[mask > 0.5] = 1
        return hard_mask
    else:
        return mask


def align(length):
    tmp = 2
    while length > tmp:
        tmp = tmp * 2
    return tmp - length


def LoadAudioTmp(y):
    tmp = []
    if (len(y) % H != 0):
        pad_size = H - len(y) % H
        tmp = np.zeros(pad_size)
        y = np.append(y, tmp)
    spec = stft(y, n_fft=FFT_SIZE, hop_length=H, win_length=FFT_SIZE)
    mag = np.abs(spec)
    mag /= np.max(mag)
    phase = np.exp(1.j * np.angle(spec))
    return mag, phase, len(tmp)


def SaveAudioTmp(mag, phase, tmplen):
    y = istft(mag * phase, hop_length=H, win_length=FFT_SIZE)
    y = y[:len(y) - tmplen]
    return y


def do_svs_with_enhanced_unet_ljj(wav_path):
    y, _ = load(wav_path, sr=16000, mono=True)
    mag, phase, tmplen = LoadAudioTmp(y)
    leng = mag.shape[1]
    # song's length >= 1024frame
    # song's length = 2^n
    tmp = np.zeros((mag.shape[0], align(leng)), dtype=np.float32)
    mag = np.concatenate((mag, tmp), axis=1)
    unet = U_Net()
    mask = ComputeMask(mag, unet, "svs_unet/unet200_trainloss_9.8154e-04_valloss_1.2863e-03.pkl", False)
    mag = mag[:, 0:leng]
    mask = mask[:, 0:leng]
    vocal = SaveAudioTmp(mag * (1. - mask), phase, tmplen)
    nonvocal = SaveAudioTmp(mag * mask, phase, tmplen)
    # librosa.output.write_wav(wav_path.replace('.wav', '.vocal.wav'), vocal, 16000)
    # librosa.output.write_wav(wav_path.replace('.wav', '.novocal.wav'), nonvocal, 16000)
    return vocal, nonvocal
