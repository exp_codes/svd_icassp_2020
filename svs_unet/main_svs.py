from librosa.core import load

from svs_unet.enhanced_unet_ljj import do_svs_with_enhanced_unet_ljj


def do_svs(wav_path, SVS):
    if SVS == 'unet':
        vocal, novocal = do_svs_with_enhanced_unet_ljj(wav_path)
    else:
        vocal, _ = load(wav_path, sr=16000, mono=True)
    return vocal, 16000

if __name__ == '__main__':
    do_svs('../test.wav', 'unet')
