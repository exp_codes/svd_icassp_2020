import os
import pickle

from CNN_clf import CNN_model
from LRCN_clf import LRCN_model
from LSTM_clf import LSTM_model
from process_dataset import DataSet_Processor

time_step = 29
for feat_item in ['mfcc', 'lpcc', 'chroma', 'mfcc,lpcc', 'mfcc,chroma', 'lpcc,chroma', 'mfcc,lpcc,chroma']:
    extract_feat = feat_item
    for dataset_item in ['Jamendo', 'RWC']:
        dataset_dir = '../SVD_5in1/%s' % (dataset_item)
        for vocal_spe_item in ['unet']:
            vocal_sep_method = vocal_spe_item
            for model_item in ['lstm', 'cnn', 'lrcn']:
                USING = model_item
                TEMP = False
                temp_pkl_path = 'pkl/%s.%s.%s.temp.pkl' % (dataset_item, extract_feat.replace(',', '_'), vocal_spe_item)
                if TEMP or not os.path.isfile(temp_pkl_path):
                    dp_dataset_dict = DataSet_Processor(dataset_dir, vocal_sep_method, extract_feat).do_SVS_get_feats()
                    with open(temp_pkl_path, "wb") as f:
                        pickle.dump(dp_dataset_dict, f)
                else:
                    with open(temp_pkl_path, 'rb') as f:
                        dp_dataset_dict = pickle.load(f)
                with open('results.txt', 'a')as res_txt:
                    res_txt.write('feats:%s\tdataset:%s\tSVS:%s\tmodel:%s\t\n' % (
                        extract_feat, dataset_item, vocal_spe_item, model_item))
                if USING == 'cnn':
                    cnn_model = CNN_model(dp_dataset_dict, 1, time_step)
                    cnn_model.eval_model()
                elif USING == 'lstm':
                    lstm_model = LSTM_model(dp_dataset_dict, 1, time_step)
                    lstm_model.eval_model()
                elif USING == 'lrcn':
                    lrcn_model = LRCN_model(dp_dataset_dict, 1, time_step)
                    lrcn_model.eval_model()

os.system('git add -A')
os.system('git commit -m "auto commit after collect results"')
os.system('git push')
