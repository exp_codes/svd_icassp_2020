# SVD_for_paper

**Date:2019.10.08 Author:Xulong Zhang**

## main.py 


## 安装依赖

@windows

    * python 3.7
    * pip3 install https://download.pytorch.org/whl/cu90/torch-1.1.0-cp37-cp37m-win_amd64.whl
    * pip3 install https://download.pytorch.org/whl/cu90/torchvision-0.3.0-cp37-cp37m-win_amd64.whl
    * pip install hmmlearn-0.2.2-cp37-cp37m-win_amd64.whl

## 常量

* wav采样率16k
 

## 实验：有无音色特征的对比

## 异常

* Jamendo数据集 test
    * wav文件03 - Say me Good Bye.wav 中间空白，音频大于标注
    * lab文件03 - Si Dieu.lab 标注大于音频
