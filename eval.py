import os

import pycrfsuite

import numpy as np
import scipy.signal as signal
from hmmlearn import hmm


def get_evaluation_f(predict, true):
    total = len(true)
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    '''
    TP:method says its positive and ground truth agrees
    TN:method says its negative and ground truth agrees
    FP:method says its positive  ground truth disagrees
    FN method says its negative ground truth disagrees
    '''
    for pre, tru in zip(predict, true):
        if pre == tru and pre == 1:
            TP += 1
        elif pre == tru and pre == 0:
            TN += 1
        elif pre != tru and pre == 1:
            FP += 1
        elif pre != tru and pre == 0:
            FN += 1
        else:
            raise Exception("error...")

    accuracy = (TP + TN) / 1.0 / total
    precision = TP / 1.0 / (TP + FP)
    recall = TP / 1.0 / (TP + FN)
    specificity = TN / 1.0 / (TN + FP)
    f1measure = 2 * precision * recall / 1.0 / (precision + recall)
    return accuracy, precision, recall, f1measure, specificity


def mid_filter(list_sig, num=3):
    return signal.medfilt(list_sig, num)


def hmm_smoothing(predictY_prob):
    np.random.seed(1007)
    model = hmm.GMMHMM(n_components=2, n_iter=40, n_mix=45)
    model.fit(predictY_prob, )
    predictY = model.predict(predictY_prob)  # 0.871955719557 #0.841328413284
    return predictY


def crf_smth(preValidY=[['no', 'yes'], ['no', 'no', 'yes']], validY=[['yes', 'yes'], ['no', 'no', 'yes']],
             preTestY=[['no', 'yes'], ['no', 'no', 'yes']], retrain=1):
    print(preValidY)
    print(validY)
    crf_model = 'saved_model/crf.model'
    if retrain == 1 or not os.path.isfile(crf_model):
        trainer = pycrfsuite.Trainer(verbose=False)
        for pr, va in zip(preValidY, validY):
            trainer.append(pr, va)
        trainer.set_params({'c1': 0.1, 'c2': 0.01, 'max_iterations': 2000, 'feature.possible_transitions': True})
        trainer.train(crf_model)
    tagger = pycrfsuite.Tagger()
    tagger.open(crf_model)
    crf_pre = []
    for preYseq in preTestY:
        yseq = tagger.tag(preYseq)
        crf_pre.extend(yseq)
    final = [int(item) for item in crf_pre]
    print(final)
    return final