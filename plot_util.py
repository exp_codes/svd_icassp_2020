from feat_extract.main_feat import do_extract_feats
from process_dataset import do_get_label
from svs_unet.main_svs import do_svs
import matplotlib.pyplot as plt
tag_path='test.lab'
wav_path = 'test.wav'
WIN_SIZE = 0.04
HOP_LEN = 0.02
time_step=29
vocal_nonvocal=''

wav_data, fs = do_svs(wav_path, 'unet')
feat_array = do_extract_feats(wav_data, fs, WIN_SIZE, HOP_LEN, 'mfcc,lpcc,chroma').T
tag_array=do_get_label(tag_path,HOP_LEN)
for item in range(0,feat_array.shape[1],time_step):
    lst_block=list(tag_array[item:item+time_step])
    if lst_block.count(1)==time_step:
        vocal_nonvocal='vocal'
    elif lst_block.count(0)==time_step:
        vocal_nonvocal='novocal'
    else:
        vocal_nonvocal = 'mix'
    plt.imshow(feat_array[:,item:item+time_step])
    plt.savefig('test_fig/%s_%08d.png'%(vocal_nonvocal,item))
print('test')


