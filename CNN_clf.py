import os

import numpy
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Flatten
from keras.layers.convolutional import Conv1D, MaxPooling1D
from keras.models import Sequential, load_model

from eval import mid_filter, get_evaluation_f


class CNN_model:
    def __init__(self, dataset_dict, retrain, time_step):
        self.retrain = retrain
        self.model_saved_path = 'saved_model/cnn.model'
        self.model_weights_saved_path = self.model_saved_path.replace('.model', '.weights')
        self.time_step = time_step
        self.dataset_dict = dataset_dict
        self.extract_dataset_data_label()

    def extract_dataset_data_label(self):
        self.trainX, self.trainY = self.get_step_data(self.dataset_dict['train_data'], self.dataset_dict['train_label'])
        self.testX, self.testY = self.get_step_data(self.dataset_dict['test_data'], self.dataset_dict['test_label'])
        self.validX, self.validY = self.get_step_data(self.dataset_dict['valid_data'], self.dataset_dict['valid_label'])
        return 0

    def get_step_data(self, dataX, dataY):
        finalX = []
        finalY = []
        feat_dim = len(dataX[0])
        for item_y in range(0, len(dataY), self.time_step):
            if list(dataY[item_y:item_y + self.time_step]).count(1) == self.time_step:
                finalX.append(dataX[item_y:item_y + self.time_step])
                finalY.append(1)
            if list(dataY[item_y:item_y + self.time_step]).count(0) == self.time_step:
                finalX.append(dataX[item_y:item_y + self.time_step])
                finalY.append(0)
        finalX = numpy.array(finalX)
        finalY = numpy.array(finalY)
        finalX = numpy.reshape(finalX, (-1, self.time_step, feat_dim))
        return finalX, finalY

    def build_load_model(self):
        if self.retrain == 1 or not os.path.isfile(self.model_saved_path):
            model = Sequential()
            feat_dim = numpy.shape(self.trainX)[-1]
            VERBOSE = 1
            model.add(Conv1D(4, 4, input_shape=(self.time_step, feat_dim), activation='relu'))
            model.add(MaxPooling1D(2))
            model.add(Conv1D(4, 4, activation='relu'))
            model.add(MaxPooling1D(2))
            model.add(Flatten())
            model.add(Dense(1, activation='sigmoid'))
            model.compile(loss='binary_crossentropy',
                          optimizer='adam',
                          metrics=['binary_accuracy'])
            print(model.summary())
            callbacks = [EarlyStopping(monitor='val_loss', patience=10, verbose=0)]
            model.fit(self.trainX, self.trainY, batch_size=128, nb_epoch=100000, callbacks=callbacks,
                      validation_data=(self.validX, self.validY),
                      verbose=VERBOSE)
            model.save(self.model_saved_path)
            model.save_weights(self.model_weights_saved_path)
        elif self.retrain != 1 and os.path.isfile(self.model_saved_path) and os.path.isfile(
                self.model_weights_saved_path):
            model = load_model(self.model_saved_path)
            model.load_weights(self.model_weights_saved_path)
            model.compile(loss='binary_crossentropy',
                          optimizer='adam',
                          metrics=['binary_accuracy'])
        else:
            raise Exception('error')

        return model

    def eval_model(self):
        model = self.build_load_model()
        loss_and_metrics = model.evaluate(self.testX, self.testY, batch_size=128, verbose=0)

        print('loss_and_metrics')
        print(loss_and_metrics)
        predictY = model.predict_classes(self.testX)
        print('predict|test')
        print(get_evaluation_f(predictY, self.testY))
        predictY_prob = model.predict_proba(self.testX)
        predictvalidY = model.predict_classes(self.validX)
        predictvalidY_prob = model.predict_proba(self.validX)
        newPreY = []
        for item in predictY:
            newPreY.extend(list(item))
        # use median filter smooth predict result
        smoothed_predictY = mid_filter(newPreY, 3)
        print('smooth predict|test')
        print(get_evaluation_f(smoothed_predictY, self.testY))
        with open('results.txt', 'a')as res_txt:
            res_txt.write('loss_and_metrics:%.3f,%.3f\n'% (loss_and_metrics[0],loss_and_metrics[1]))
            # res_txt.write('predict|test\taccuracy\t precision\t recall\t f1measure\tspecificity\n\t\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n'%get_evaluation_f(predictY, self.testY))
            res_txt.write(
                'smooth predict|test\taccuracy\t precision\t recall\t f1measure\tspecificity\n\t\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n' % get_evaluation_f(
                    smoothed_predictY, self.testY))
