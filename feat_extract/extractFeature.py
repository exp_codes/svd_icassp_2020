import librosa
import numpy
import numpy as np
from scipy.fftpack import dct


def get_audio_feature(audio_data, sample_rate, WIN_SIZE=0.040, HOP_LEN=0.020,
                      feat_list=['mfcc', 'lpcc', 'chroma', 'zcr', 'rmse', 'centroid', 'rolloff', 'poly', 'contrast',
                                 'bandwith', 'mfcc_delta_delta', 'mfcc_delta']):
    WIN_SIZE = int(WIN_SIZE * sample_rate)
    HOP_LEN = int(HOP_LEN * sample_rate)
    # WIN_SIZE need equls to HOP_LEN ie. no overlaps between frames.
    all_feat = []
    '''
    mfcc,chroma,zcr,rmse,centroid,rolloff,poly,contrast,bandwith,mfcc_delta_delta,mfcc_delta,lpcc

    '''
    if 'mfcc' in feat_list:
        featMFCC = librosa.feature.mfcc(y=audio_data, sr=sample_rate, n_mfcc=26,
                                        n_fft=WIN_SIZE,
                                        hop_length=HOP_LEN, )
        all_feat.append(featMFCC)

    if 'chroma' in feat_list:
        featChroma = librosa.feature.chroma_stft(y=audio_data, sr=sample_rate, hop_length=HOP_LEN,
                                                 n_fft=WIN_SIZE)
        all_feat.append(featChroma)
    if 'zcr' in feat_list:
        featZCR = librosa.feature.zero_crossing_rate(audio_data, hop_length=HOP_LEN)
        all_feat.append(featZCR)

    if 'rmse' in feat_list:
        featRMSE = librosa.feature.rms(audio_data, frame_length=WIN_SIZE,
                                       hop_length=HOP_LEN)
        all_feat.append(featRMSE)

    if 'centroid' in feat_list:
        featCentroid = librosa.feature.spectral_centroid(audio_data, sample_rate, hop_length=HOP_LEN,
                                                         win_length=WIN_SIZE)
        all_feat.append(featCentroid)

    if 'rolloff' in feat_list:
        featRolloff = librosa.feature.spectral_rolloff(audio_data, sample_rate, hop_length=HOP_LEN,
                                                       n_fft=WIN_SIZE)
        all_feat.append(featRolloff)

    if 'poly' in feat_list:
        featPoly = librosa.feature.poly_features(audio_data, sample_rate, hop_length=HOP_LEN,
                                                 n_fft=WIN_SIZE)
        all_feat.append(featPoly)

    if 'contrast' in feat_list:
        featContrast = librosa.feature.spectral_contrast(audio_data, sample_rate, hop_length=HOP_LEN,
                                                         n_fft=WIN_SIZE)
        all_feat.append(featContrast)

    if 'bandwith' in feat_list:
        featBandwith = librosa.feature.spectral_bandwidth(audio_data, sample_rate,
                                                          hop_length=HOP_LEN,
                                                          n_fft=WIN_SIZE)
        all_feat.append(featBandwith)

    if 'mfcc_delta_delta' in feat_list:
        featMFCC = librosa.feature.mfcc(y=audio_data, sr=sample_rate, n_mfcc=26,
                                        n_fft=WIN_SIZE,
                                        hop_length=HOP_LEN, )
        featMFCC_delta_delta = librosa.feature.delta(featMFCC, order=2)
        all_feat.append(featMFCC_delta_delta)

    if 'mfcc_delta' in feat_list:
        featMFCC = librosa.feature.mfcc(y=audio_data, sr=sample_rate, n_mfcc=26,
                                        n_fft=WIN_SIZE,
                                        hop_length=HOP_LEN, )
        featMFCC_delta = librosa.feature.delta(featMFCC)
        all_feat.append(featMFCC_delta)

    if 'lpcc' in feat_list:
        # Pad the window out to n_fft size
        # Pad the time series so that frames are centered
        audio_data = np.pad(audio_data, int(WIN_SIZE // 2), mode='reflect')
        y_framed = librosa.util.frame(audio_data, WIN_SIZE, HOP_LEN).T
        featLPCC = []
        for frame in y_framed:
            try:
                lpc_frame = librosa.core.lpc(frame, 12)
            except:
                lpc_frame=np.ones(13)
            # 对lpc进行倒谱分析
            fft_lpc = np.fft.fft(lpc_frame)
            log_fft = np.log(fft_lpc)
            ceps_lpc = np.fft.ifft(log_fft).real
            lpcc_frame = dct(ceps_lpc, 1)
            featLPCC.append(np.nan_to_num(lpcc_frame))
        featLPCC = np.concatenate([featLPCC]).T
        all_feat.append(featLPCC)

    combineFeat = numpy.vstack(all_feat).T
    norm_combineFeat = combineFeat / abs(combineFeat).max(axis=0)
    return norm_combineFeat


if __name__ == '__main__':
    wav_path = '../test.wav'
    vocal, sr = librosa.core.load(wav_path, sr=16000, mono=True)
    combineFeat = get_audio_feature(vocal, sr, )
