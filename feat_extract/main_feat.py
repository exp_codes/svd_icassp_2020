from feat_extract.extractFeature import get_audio_feature


def do_extract_feats(wav_data, fs, WIN_SIZE, HOP_LEN, feats):
    feat_list = [feat.strip(' ') for feat in feats.split(',')]
    audio_feat=get_audio_feature(wav_data, fs, WIN_SIZE, HOP_LEN, feat_list)
    return audio_feat
